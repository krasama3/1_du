﻿using System;

namespace hadani_cisla
{
    class Program
    {
        static void Main(string[] args)
        {
            //int j = 100; //horní hranice
            Boolean pokracovat;
            int x;//počet iterací
            //

            int phorni;
            int pdolni;
            int stred;

            while (true) // první vrstva programu která opakuje hádání 
            {

                Console.WriteLine("Zadejte dolní a horní hranici");
                string hornidolni = Console.ReadLine();
                string[] segmenty = hornidolni.Split(" ");
                //ověření vstupu 
                if (segmenty.Length < 2 || segmenty[1] == "")
                {
                    Console.WriteLine("Chybný vstup");
                    Console.ReadLine();
                    continue;
                }
                //je to číslo?
                bool dolnicislo = int.TryParse(segmenty[0], out int dolni);
                bool hornicislo = int.TryParse(segmenty[1], out int horni);

                stred = (dolni + horni) / 2;
                phorni = horni + 1; //aby program byl schopen najít i hranice jako hádané číslo
                pdolni = dolni - 1;

                Console.WriteLine($"Myslete si číslo od {dolni} do {horni}, pro pokračování stiskněte libovolnou klávesu");

                Console.ReadKey();

                pokracovat = true;
                x = 0;

                while (pokracovat == true) //druhá vrstva programu která obstarává iterace
                {

                    x++;



                    Console.WriteLine($"Je Vaše číslo větší, menší nebo rovno {stred}?");
                    Console.WriteLine("Pokud je číslo větší zadejte znak **v**");
                    Console.WriteLine("Pokud je číslo menší zadejte znak **m**");
                    Console.WriteLine("Pokud se číslo rovná Vašemu číslu zadejte znak **r**");
                    string odpoved = Console.ReadLine();

                    if (odpoved == "v")
                    {
                        //phorni = phorni;
                        pdolni = stred;
                        stred = (pdolni + phorni) / 2;

                    }
                    else if (odpoved == "m")
                    {
                        phorni = stred;
                        //pdolni = pdolni;
                        stred = (pdolni + phorni) / 2;
                    }
                    else if (odpoved == "r")
                    {
                        Console.WriteLine($"Číslo jsem uhádl po {x} pokusu");
                        Console.ReadKey();
                        Console.WriteLine("Chcete ukončit program?:(y/n)");
                        string konecString = Console.ReadLine();

                        if (konecString == "y" || konecString == "Y")
                        {
                            return;
                        }
                        else if (konecString == "n" || konecString == "N")
                        {
                            pokracovat = false;
                        }
                    }
                    else
                    {
                        x--; //odečítání chybného pokusu
                        Console.WriteLine("Nezadal jste požadovaný znak");
                        Console.ReadKey();
                        continue;
                    }
                }
            }
        }
    }
}
